import React from 'react';
import css from './LoginPage.styl';

// const Component = React.Component;
const { Component } = React;

export default class SubmitForm extends Component 
{

  constructor(props) 
  {
    console.log("log: LoginPage.js constructor");
    super(props);
    this.state = 
    { 
      inputValue: '',
    }
  }

  onUserInput = (event) => 
  {
    this.setState({ inputValue: event.target.value })
  }

  onSubmitName = () => 
  {
    this.props.onSubmit(this.state.inputValue);
  }

  userInputNameClicked = (event) =>
  {
     if (event.key === 'Enter') 
     {
        this.props.onSubmit(this.state.inputValue);
     }
  }

  render()
  {
    let showError = true;

    if(this.props.loginStatusMessage ==='')
      showError = false;

    return (
      <div className={css.container}>

        <div className={css.welcomeDiv}>Dear user, to enjoy our chat room please enter your name</div>
        <div>
          <input className={ css.userInputName } onChange={ this.onUserInput } onKeyPress={this.userInputNameClicked} value={ this.state.inputValue } />
          <div className={ css.submitButton } onClick={ this.onSubmitName }>Submit</div>
        </div>
        <div className={css.errorMessage} style={showError ? {visibility:''}: {visibility:'hidden'}}>{this.props.loginStatusMessage}</div>
      </div>

      
    );
  } 

  
}