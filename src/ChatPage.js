import React from 'react';
import io from 'socket.io-client';
import css from './ChatPage.styl';
import LoginPage from './LoginPage.js';

// const Component = React.Component;
const { Component } = React;

export default class Page extends Component 
{

  constructor(props) 
  {
    super(props);
    this.state = 
    { 
      loginStatus : {isAvailable:false, loginMessage:''},
      currentUserName: '',
      messages :[],
      inputChatMessage : '',
      usernamesArray :[],
      inputValue: ''
    }
  }

  componentDidMount() 
  {
    this.socket = io('http://localhost:3001');
    this.socket.on('sendingMessageToClient', (receivingMessage) => 
    {
      let messagesTemp = [];
          receivingMessage.map( (value)=>{
            if(value.messageText != 'undefined')
            {
              value.messageText = value.messageText.split(":-??").join("<span><img src='/assets/106.gif'><span>");
              value.messageText = value.messageText.split(";-)").join("<span><img src='/assets/winking.gif'><span>");
              value.messageText = value.messageText.split(";)").join("<span><img src='/assets/winking.gif'><span>");
              value.messageText = value.messageText.split(":-D").join("<span><img src='/assets/big-grin.gif'><span>");
              value.messageText = value.messageText.split(":-d").join("<span><img src='/assets/big-grin.gif'><span>");
              value.messageText = value.messageText.split(":D").join("<span><img src='/assets/big-grin.gif'><span>");
              value.messageText = value.messageText.split(":d").join("<span><img src='/assets/big-grin.gif'><span>");
              value.messageText = value.messageText.split(":-x").join("<span><img src='/assets/love-struck.gif'><span>");
              value.messageText = value.messageText.split(":-X").join("<span><img src='/assets/love-struck.gif'><span>");
              value.messageText = value.messageText.split(":x").join("<span><img src='/assets/love-struck.gif'><span>");
              value.messageText = value.messageText.split(":X").join("<span><img src='/assets/love-struck.gif'><span>");
              value.messageText = value.messageText.split(":j").join("<span><img src='/assets/oh-go-on.gif'><span>");
              value.messageText = value.messageText.split(":J").join("<span><img src='/assets/oh-go-on.gif'><span>");
              value.messageText = value.messageText.split(":-j").join("<span><img src='/assets/oh-go-on.gif'><span>");
              value.messageText = value.messageText.split(":-J").join("<span><img src='/assets/oh-go-on.gif'><span>");
              value.messageText = value.messageText.split(":-o").join("<span><img src='/assets/suprise.gif'><span>");
              value.messageText = value.messageText.split(":-O").join("<span><img src='/assets/suprise.gif'><span>");
              value.messageText = value.messageText.split(":o").join("<span><img src='/assets/suprise.gif'><span>");
              value.messageText = value.messageText.split(":O").join("<span><img src='/assets/suprise.gif'><span>");
              value.messageText = value.messageText.split(":?").join("<span><img src='/assets/thinking.gif'><span>");
              value.messageText = value.messageText.split(":-?").join("<span><img src='/assets/thinking.gif'><span>");
              value.messageText = value.messageText.split(":-))").join("<span><img src='/assets/laughing.gif'><span>");
              value.messageText = value.messageText.split(":))").join("<span><img src='/assets/laughing.gif'><span>");
              value.messageText = value.messageText.split(":((").join("<span><img src='/assets/crying.gif'><span>");
              value.messageText  = <span dangerouslySetInnerHTML={{ __html: value.messageText}}></span>;
              messagesTemp.push(value);
            }
          });

        var audio = new Audio('/assets/receive-notification.mp3');
        audio.play(); 

        this.setState({messages:messagesTemp});
    })

    this.socket.on('loginServerStatus', (loginStatus) => 
    {
      let loginStatusTemp = loginStatus;
      this.setState( { loginStatus:loginStatusTemp } ); 
    })

    this.socket.on('userChangeHandler', (usernamesArray) => 
    {
      let usernamesArrayTemp = usernamesArray;
      this.setState({usernamesArray : usernamesArrayTemp});      
    })
  }

  submitName = (name) => 
  {
    if(name !== '') 
    {
      this.setState({ currentUserName : name });
      this.socket.emit("login", name);
    }
  }


  render()
  {
    return this.renderSuitableContent();
  }


  renderSuitableContent()
  {
    if(this.state.loginStatus.isAvailable)
    {
      return this.renderChatPage();
    }
    else
    {
      let loginStatusMessage  =  this.state.loginStatus;
      if(!this.state.loginStatus.isAvailable && this.state.loginStatus.loginMessage=='' )
            return (<LoginPage onSubmit={ this.submitName } loginStatusMessage='' />);
      if(!this.state.loginStatus.isAvailable && this.state.loginStatus.loginMessage !='' )
          return (<LoginPage onSubmit={ this.submitName } loginStatusMessage='Currently there is a user with entered Name' />);  
    }
  }

renderChatPage()
{
     return (
        <div className={css.mainDiv}>
          <div className={css.welcomeDiv}>Hello <span className={css.welcomeDivUserName} style={{fontWeight:'bold'}}>{this.state.currentUserName}</span>, Welcome to Chat Room</div>

          <div className={css.chat_room_top_side}>
            <div className={css.chat_room_top_left_side} >
              
              {
                   this.state.usernamesArray.map( (value)=>{
                     return(
                      <div className={css.messageContainer}>
                        <div className={css.messageImageProfile} style={{width:'15%'}}></div>
                        <div className={css.messageContent}  >
                          <div className={css.messageUserName} style={{color:'white', fontWeight:'bold'}} >{value}</div>
                        </div>
                      </div>
                   )
                   })
              }


            </div>
            <div className={css.chat_room_top_right_side}>
              {
                this.state.messages.map( (value)=>{
                    
                  let isMessageFromCurrentUser = true;
                  if(this.state.currentUserName != value.userName)
                  {
                     isMessageFromCurrentUser = false;
                  }
                      
                  return ( 
                    <div className={css.messageContainer}  style={isMessageFromCurrentUser ? {direction: 'rtl'}: {color:''}}>
                      <div className={css.messageImageProfile}></div>
                      <div className={css.messageContent} style={isMessageFromCurrentUser ? {	backgroundColor:'#75c5ff'} : {} }>
                        <div className={css.messageUserName} style={isMessageFromCurrentUser ? {color:'red', direction: 'ltr'}: {}}>{value.userName}</div>
                        <div className={css.messageText} style={{direction: 'ltr'}}>{value.messageText}</div>


                      </div>
                    </div>
                    
                  )
                  })
              }

            </div>
        </div>

        <div className={css.chat_room_bottom_side}>
          <input type="text" className={css.message_input} onChange={ this.onUserInput } onKeyPress={this.userInputMessageTextClicked} value={ this.state.inputChatMessage }></input>
          <div className={css.send_button} onClick={ this.userInputMessageButtonClicked }></div>
        </div>
      </div>
     );
}

onUserInput = (event) => 
{
  this.setState({ inputChatMessage: event.target.value })
}

userInputMessageTextClicked = (event) =>
{
     if (event.key === 'Enter') 
     {
       this.sendMessageToServer();
     }
}

userInputMessageButtonClicked = () =>
{
  this.sendMessageToServer();
}

sendMessageToServer()
{
  if( this.state.inputChatMessage != '')
  {
      let message = { userName:this.state.currentUserName ,  messageText: this.state.inputChatMessage}
      this.socket.emit("sendMessageFromClient", message);
      this.setState({inputChatMessage:''});
  }
}

}