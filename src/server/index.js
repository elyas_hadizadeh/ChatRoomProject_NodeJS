let express = require('express')
let app = express();
let http = require('http').Server(app);
let io = require('socket.io')(http);

let count = 0;
let users = 
{
  done: [],
  unDone: []
}




let userNames = [];
let usernameAndId = {};

let allReceiveMessages = [];

function isSuchUser(username)
{
    for(key in usernameAndId)
    {
      if(usernameAndId[key] == username )
        return true;
    }
       return false;
}

io.on('connection', (socket) => 
{
  let today = new Date();
  let dd = today.getDate();
  let mm = today.getMonth()+1; //January is 0!
  let yyyy = today.getFullYear();
  let currentDate = yyyy + '/'+ mm + '/' +dd;
  console.log(  currentDate  + " ,new user has entered:");

  var clientIp = socket.request.connection.remoteAddress;

  console.log("Socket Id: " + socket.id);
  console.log("IP Address: " + socket.request.connection._peername.address);
  console.log("IP Type: " + socket.request.connection._peername.family);
  console.log("Port Number: " + socket.request.connection._peername.port);

  socket.on("sendMessageFromClient", message => 
  {
    console.log(message);
    allReceiveMessages.push(message);
    socket.emit("sendingMessageToClient", allReceiveMessages);
    socket.broadcast.emit("sendingMessageToClient", allReceiveMessages );
  });



  socket.on("toggleuser", (username) => 
  {
    console.log('here here');
    let isDone = isUserDone(username);
    toggleUser(isDone, username);
    io.sockets.emit("userStatus", { users: users });
  });


function updateUserArrayListOnClientSide()
{
        let usernamesArray = [];
      for(key in usernameAndId)
        usernamesArray.push(usernameAndId[key]);
      socket.broadcast.emit("userChangeHandler",usernamesArray);
      socket.emit("userChangeHandler",usernamesArray);
      socket.emit("sendingMessageToClient", allReceiveMessages );
}

  socket.on("login", (username) => 
  {
    // for(key in userNameAndIds)
    //   console.log(key,userNameAndIds[key]);
    

    // if( Object.values(usernameAndId).indexOf('username') == -1 )
    // {

    if( !isSuchUser(username) )
    {
      usernameAndId[socket.id] = username;
      let loginStatus = {isAvailable: true, loginMessage: 'welocme'};

      socket.emit("loginServerStatus", loginStatus);
      updateUserArrayListOnClientSide();


    }
    else
    {
      let loginStatus = {isAvailable: false, loginMessage: 'Currently there is a user with entered Name'};
      socket.emit("loginServerStatus", loginStatus);
    }


    // if( (userNames.indexOf(username) == -1) )
    // {
    //   console.log("**********************Inserting into usernames");
    //   userNames.push(username);
    //   console.log(userNames);
    //   let loginStatus = true;
    //   socket.emit("loginServerStatus", loginStatus);
    // }
    // else
    // {
    //   console.log("**********************Error in: Inserting into usernames");
    //   let loginStatus = false;
    //   socket.emit("loginServerStatus", loginStatus);
    // }
  })



  socket.on("userjoin", (username) => 
  {
    removeExistingUser(username);
    let newUser = 
    {
      name: username,
      id: Date.now()
    }
    
    users.unDone = users.unDone.concat(newUser)

    response = 
    {
      users: users,
      yourUser: newUser
    }
    socket.emit("userStatus", response)
    socket.broadcast.emit("userStatus", { users: users });
  })

  socket.on('disconnect', () => {
    console.log("yeki raft birun");
    // var index = clients.indexOf(socket);

    if(usernameAndId.hasOwnProperty(socket.id))
    {
      delete usernameAndId[socket.id];
      updateUserArrayListOnClientSide();
    }

  })

})

function removeExistingUser(username) 
{
  users.unDone = users.unDone.filter( (userInfo) => 
  {
      return !(userInfo.name === username)
    })
  users.done = users.done.filter((userInfo) => {
    return !(userInfo.name === username)
  })
}

function isUserDone(username) 
{
  return users.done.find( (user) => user.id == username.id );
}

function toggleUser(isDone, userInfo) 
{
  console.log('in console log function');
  console.log(isDone);
  if(isDone) 
  {
    users.done = users.done.filter( (user) => 
    {
      return user.id !== userInfo.id
    })
    users.unDone.push(userInfo);
  } else 
  {
    users.unDone = users.unDone.filter( (user) => 
    {
      return user.id !== userInfo.id
    })
    users.done.push(userInfo);
  }
}

function getMessageFromClient(message)
{

}

http.listen(3001, function()
{
  console.log('listening on *:3001');
});