import React from 'react';
import ChatPage from './ChatPage';

const Component = React.Component;

export default class App extends Component 
{

  constructor(prop) 
  {
    super(prop);
  }

  render()
  {
    console.log("log: App.js");
    return ( <ChatPage/> );
  }

}